package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"

	"github.com/gofiber/contrib/websocket"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
)

type eventData struct {
	RegisteredAt time.Time `json:"registeredAt"`
}

// dataset correponds to the [Object notation](https://www.chartjs.org/docs/latest/general/data-structures.html#object-2) of data that can be sent to Chart.js. Using this notation, data lookups and updates in Go are faster
type dataset struct {
	Label        string               `json:"label"` // Event type
	Data         map[time.Time]uint64 `json:"data"`
	BarThickness int                  `json:"barThickness"`
}

const numberOfTicksToShow = 100

var totalCounts = make(map[string]*uint64) // Note: pointer values so we can use atomic counters per entry instead of for the enitre map
var totalCountsMu sync.Mutex               // Mutex to prevent concurrent writing + reading/writing

var datasets = make([]dataset, 0) // Make the slice, in order to return an empty array instead of null when marshalled as JSON
var datasetsMu sync.Mutex         // Mutex to prevent concurrent reading/writing the dataset

// times is a slice to keep track of the n most recent times for which messages from the time stream are received
var times = make([]time.Time, 0, numberOfTicksToShow)
var timesMu sync.Mutex

var visitors = make(map[string]*websocket.Conn)

// Set up a Fiber template engine
var engine = html.New("./views", ".html")

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", fiber.Map{
			"visitorID": randomString(16),
		})
	})

	// Subscribe to NATS/JetStream to listen to events

	// Connect to the NATS server
	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		log.Println(err)
		return
	}

	// Create a JetStream instance for the NATS connection
	js, err := jetstream.New(nc)
	if err != nil {
		log.Println(err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	// Get the time stream
	timeStream, err := js.Stream(ctx, "time")
	if err != nil {
		log.Println(err)
		return
	}

	// Create a consumer
	timeCons, err := timeStream.CreateOrUpdateConsumer(ctx, jetstream.ConsumerConfig{
		Durable:   "generator-dashboard", // Note: this line to makes the consumer durable
		AckPolicy: jetstream.AckExplicitPolicy,
	})
	if err != nil {
		log.Println(err)
		return
	}

	// Get the events stream
	eventsStream, err := js.Stream(ctx, "events")
	if err != nil {
		log.Println(err)
		return
	}

	// Create a consumer
	eventsCons, err := eventsStream.CreateOrUpdateConsumer(ctx, jetstream.ConsumerConfig{
		Durable:   "generator-dashboard", // Note: this line to makes the consumer durable
		AckPolicy: jetstream.AckExplicitPolicy,
	})
	if err != nil {
		log.Println(err)
		return
	}

	// Consume messages from the time consumer in a callback
	timeC, err := timeCons.Consume(func(msg jetstream.Msg) {
		// Ack the message
		msg.Ack()
		// log.Printf("received JetStream time message: %s\n", msg.Data())

		// Decode the event data
		ts, err := time.Parse(time.RFC3339Nano, string(msg.Data()))
		if err != nil {
			log.Println("cannot unmarshal time event data, skipping")
			return
		}

		timesMu.Lock()

		// Add the timestamp to the times slice
		times = append(times, ts)

		// If more than `numberOfTicksToShow` entries, remove the oldest timestamp from the slice. Note: this is not necessarily the earliest time in terms of time comparison, but the earliest received time event, which is preferred in case the time stream is reset/purged during execution
		if len(times) > numberOfTicksToShow {
			times = times[1:]
		}

		timesMu.Unlock()
	})
	if err != nil {
		log.Println(err)
		return
	}

	defer timeC.Stop()

	// Consume messages from the events consumer in a callback
	eventsC, err := eventsCons.Consume(func(msg jetstream.Msg) {
		// Ack the message
		msg.Ack()
		// log.Printf("received JetStream events message: %s\n", msg.Data())

		sub := msg.Subject()

		// Note: we write to the map only when appending a new event type. The counters themselves have individual mutexes
		var counter *uint64
		totalCountsMu.Lock()
		if counter = totalCounts[sub]; counter == nil {
			counter = new(uint64)
			totalCounts[sub] = counter
		}
		totalCountsMu.Unlock()
		atomic.AddUint64(counter, 1)

		// Decode the event data
		var data eventData
		if err := json.Unmarshal(msg.Data(), &data); err != nil {
			log.Println("cannot unmarshal event data, skipping")
			return
		}

		// In case the message is out of bounds for the times, do not append it. Note: we assume the time events are in ascending order
		if len(times) > 1 && (data.RegisteredAt.Before(times[0]) || data.RegisteredAt.After(times[len(times)-1])) {
			return
		}

		// Update the datasets

		// Find the existing data for this event, if it exists
		var found bool
		for _, ds := range datasets {
			if ds.Label == sub {
				found = true

				datasetsMu.Lock()
				ds.Data[data.RegisteredAt]++
				datasetsMu.Unlock()

				break // Assume only one dataset exists with the same label
			}
		}

		// If no dataset yet exists for this event type, append a dataset to the datasets
		if !found {
			datasetsMu.Lock()
			datasets = append(datasets, dataset{
				Label:        sub,
				Data:         map[time.Time]uint64{data.RegisteredAt: 1},
				BarThickness: 4, // IMPROVE: omit here and append in frontend?
			})
			datasetsMu.Unlock()
		}
	})
	if err != nil {
		log.Println(err)
		return
	}

	defer eventsC.Stop()

	// Output the counters every second
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	go func() {
		for range ticker.C {
			var lower, upper time.Time
			// Loop through all datasets and remove the data that is out of bounds according to the times slice
			if len(times) > 1 {
				if lower, upper = times[0], times[len(times)-1]; !lower.After(upper) { // Note: make sture that lower <= upper, since the time stream could have been reset/purged during execution
					for _, dataset := range datasets {
						for ts := range dataset.Data {
							if ts.Before(lower) || ts.After(upper) {
								datasetsMu.Lock()
								delete(dataset.Data, ts)
								datasetsMu.Unlock()
							}
						}
					}
				}
			}

			// Encode the stats as json
			msg, err := json.Marshal(struct {
				TotalCounts map[string]*uint64 `json:"totalCounts"`
				MinTime     time.Time          `json:"minTime"`
				MaxTime     time.Time          `json:"maxTime"`
				Datasets    []dataset          `json:"datasets"`
			}{
				totalCounts,
				lower,
				upper,
				datasets,
			})
			if err != nil {
				log.Println(err)
				return
			}

			// Send a WebSocket message to all dashboard connections with the stats
			for _, conn := range visitors {
				if err := conn.WriteMessage(1, msg); err != nil {
					log.Printf("error writing WebSocket message: %v", err)
					continue
				}
			}
		}
	}()

	// Endpoint for WebSocket messages
	app.Use("/ws", func(c *fiber.Ctx) error {
		// Note: IsWebSocketUpgrade returns true if the client requested upgrade to the WebSocket protocol
		if websocket.IsWebSocketUpgrade(c) {
			c.Locals("allowed", true)
			return c.Next()
		}

		return fiber.ErrUpgradeRequired
	})

	app.Get("/ws/:id", websocket.New(func(c *websocket.Conn) {
		visitorID := c.Params("id")

		// See https://pkg.go.dev/github.com/fasthttp/websocket?tab=doc#pkg-index for websocket.Conn bindings
	messageLoop:
		for {
			// Wait for incoming messages
			messageType, _, err := c.ReadMessage()
			if err != nil {
				// Log the error
				if websocket.IsCloseError(err, websocket.CloseAbnormalClosure, websocket.CloseGoingAway) {
					// Nothing to be done
				} else if websocket.IsUnexpectedCloseError(err, websocket.CloseAbnormalClosure, websocket.CloseGoingAway) {
					log.Printf("a visitor left the page unexpectedly: %v", err)
				} else {
					log.Println("a visitor left the page due to a read error:", err)
				}

				// Remove the visitor from the list of active visitors
				delete(visitors, visitorID)
				break
			}

			// Handle the message, based on its type, see https://www.rfc-editor.org/rfc/rfc6455.html#section-11.8
			switch messageType {
			case 9, 10:
				// Ignore ping / pong messages
				continue messageLoop

			case 8:
				// Also ignore connection close messages, which should be handled above
				continue messageLoop
			}

			// Else: add to the visitor to the list of active visitors. Note: we currently ignore the message content
			visitors[visitorID] = c
		}
	}))

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}

// randomString returns a random string with the specified length. See https://stackoverflow.com/a/31832326 for alternatives
func randomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))] // Note: seeding the rand is no longer required since Go 1.20, see https://github.com/golang/go/issues/54880 (an alternative is to use rand.New(rand.NewSource(time.Now().UnixNano())) )
	}
	return string(b)
}
