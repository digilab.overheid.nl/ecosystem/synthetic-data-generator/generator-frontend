// Imports
import './htmx.js'; // Note: imported this way, since some plugins needs the global `htmx` variable, which requires the use of an extra file according to https://htmx.org/docs/#webpack (which indeed seems to be the case)
import 'htmx.org/dist/ext/ws.js'; // WebSockets extension

import Chart from 'chart.js/auto';
import * as Utils from './chartjs-utils';

import { Settings } from 'luxon';
import 'chartjs-adapter-luxon';

Settings.defaultLocale = 'nl';

const slider = document.getElementById('speed-slider');
const description = document.getElementById('speed-description');

speeds = [
  0, // Paused
  1, // Real-time
  24, // Every hour in the simulation corresponds to one day in time
  24*60, // Every minute corresponds to one day
  60*60, // Every second corresponds to one hour (default)
  2*60*60, // Every second corresponds to two hours
  3*60*60, // Every second corresponds to three hours
];


// let tickInterval = setInterval(tick, 1000); // Note: default speed is: every second corresponds to one hour

slider.addEventListener('input', () => {
  const speed = speeds[parseInt(slider.value, 10)];

  // // Reset ticking
  // clearInterval(tickInterval);

  if (speed) {
    description.innerText = speed + ' × werkelijkheid';
    
    // tickInterval = setInterval(tick, 60*60*1000 / speed);
  } else {
    description.innerText = 'Pauze';
  }
});


// Immediately send a message once the connection is open (note: the htmx:wsOpen event bubbles to document)
document.addEventListener('htmx:wsOpen', (ev) => {
  ev.detail.socketWrapper.send('init', ev.target);
});



// Initialize the events chart
const chart = new Chart(document.getElementById('events'), {
  type: 'bar',
  data: data = {
    datasets: []
  },
  options: {
    responsive: true,
    plugins: {
      legend: {
        position: 'bottom',
        align: 'start',
        labels: {
          boxWidth: 16,
          textAlign: 'left',
        }
      }
    },
    scales: {
      x: {
        type: 'time',
        time: {
          unit: 'day',
          displayFormats: {
            hour: 'd MMMM HH:mm',
            day: 'd MMMM yyyy',
          }
        }
      },
      y: {
        beginAtZero: true,
        ticks: {
          precision: 0, // Do not show e.g. 0.5 in the y axis labels
        }
      }
    }
  },
});

const countsEl = document.getElementById('counts');

// When a message is received, update the charts
document.addEventListener('htmx:wsAfterMessage', (ev) => {
  const msg = JSON.parse(ev.detail.message);

  // Show the totalCounts counters
  var inner = '';
  Object.keys(msg.totalCounts).sort().forEach((key) => {
    inner += `<dl class="p-4 bg-slate-100 rounded-md">
        <dt class="text-sm font-medium text-slate-800">${key}</dt>
        <dd>${msg.totalCounts[key]}</dd>
      </dl>`
  });
  countsEl.innerHTML = inner;

  // Update the chart with the datasets attribute
  chart.data.datasets = msg.datasets;

  // Update the chart X axis min and max values
  chart.options.scales.x.min = msg.minTime;
  chart.options.scales.x.max = msg.maxTime;

  chart.update('none'); // Disable animations. IMPROVE: Compare the datasets with the existing ones and only update if changed?
});


// Stats

// Probability of death
// Source: https://opendata.cbs.nl/statline/#/CBS/nl/dataset/37360ned/table?fromstatweb
// Interpolated with polynomial fit of order 6 (e.g. using https://observablehq.com/@christophe-yamahata/polynomial-regression-in-javascript)
// Data: [0,0.00287],[1,0.00045],[2,0.00019],[3,0.00016],[4,0.00010],[5,0.00010],[6,0.00009],[7,0.00005],[8,0.00011],[9,0.00008],[20,0.00034],[21,0.00032],[22,0.00034],[23,0.00027],[24,0.00029],[25,0.00031],[26,0.00034],[27,0.00031],[28,0.00029],[29,0.00036],[30,0.00040],[31,0.00045],[32,0.00044],[33,0.00044],[34,0.00049],[35,0.00055],[36,0.00051],[37,0.00058],[38,0.00072],[39,0.00076],[40,0.00069],[41,0.00083],[42,0.00105],[43,0.00101],[44,0.00120],[45,0.00122],[46,0.00133],[47,0.00145],[48,0.00179],[49,0.00199],[50,0.00202],[51,0.00236],[52,0.00252],[53,0.00277],[54,0.00323],[55,0.00328],[56,0.00380],[57,0.00403],[58,0.00449],[59,0.00490],[60,0.00536],[61,0.00584],[62,0.00642],[63,0.00756],[64,0.00809],[65,0.00889],[66,0.01013],[67,0.01078],[68,0.01236],[69,0.01322],[70,0.01538],[71,0.01608],[72,0.01778],[73,0.02000],[74,0.02220],[75,0.02463],[76,0.02725],[77,0.03157],[78,0.03486],[79,0.03886],[80,0.04262],[81,0.04824],[82,0.05528],[83,0.06177],[84,0.06895],[85,0.07951],[86,0.09177],[87,0.10490],[88,0.11998],[89,0.13883],[90,0.15387],[91,0.17568],[92,0.19444],[93,0.21914],[94,0.24018],[95,0.26836],[96,0.28753],[97,0.32552],[98,0.33750]
// Resulting polynomial:
// f(x)=0.00058423-0.0000375718x+0.0000162326x^2-0.0000016991x^3+0.0000000635x^4-0.000000000974x^5+0.000000000005362x^6
const stat1Fn = (x) => 0.00058423 - 0.0000375718 * x + 0.0000162326 * (x ** 2) - 0.0000016991 * (x ** 3) + 0.0000000635 * (x ** 4) - 0.000000000974 * (x ** 5) + 0.000000000005362 * (x ** 6);

const stat1Data = [];

for (let x = 0; x < 101; x++) {
  stat1Data.push(stat1Fn(x));
}

new Chart(document.getElementById('stat-1'), {
  type: 'line',
  data: {
    labels: Array.from({ length: 101 }, (_, index) => `${index} jaar`),
    datasets: [
      {
        label: 'Kans op overlijden per jaar',
        data: stat1Data.map((val) => (val * 100)), // Multiply by 100 to get percentages
        borderColor: Utils.CHART_COLORS.blue,
        backgroundColor: Utils.transparentize(Utils.CHART_COLORS.blue, 0.5),
      }
    ]
  },
  options: {
    responsive: true,
    scales: {
      x: {
        title: {
          display: true,
          text: 'Leeftijd van de persoon',
        },
      },
      y: {
        min: 0,
        max: 100,
        offset: true,
        title: {
          display: true,
          text: 'Kans op overlijden in een jaar tijd',
        },
        ticks: {
          callback: function (value) {
            return value + '%';
          },
        },
      }
    },
    elements: {
      point: {
        radius: 0,
        hoverRadius: 4,
        hitRadius: 4,
      },
      line: {
        tension: 1,
        cubicInterpolationMode: 'monotone', // For smoother lines
      }
    },
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        callbacks: {
          label: function (context) {
            var label = context.dataset.label || '';
            if (context.parsed.y !== null) {
              label += ' ' + context.parsed.y.toFixed(2) + '%';
            }
            return label;
          }
        }
      },
    }
  },
});

// Probability of birth
// Source: https://opendata.cbs.nl/#/CBS/nl/dataset/37201/table?dl=94E25 and https://opendata.cbs.nl/#/CBS/nl/dataset/03759ned/table?dl=94E28
// Interpolated with Gaussian Bell curve fit (e.g. using https://mycurvefit.com/)
// [12.5,0.00020557],[22.5,0.00858504],[27.5,0.03612038],[32.5,0.05931208],[37.5,0.03316359],[42.5,0.00722938],[60,0.00219446]
// Resulting equation:
// y = 0.05897603*e^(-(x - 32.29439)^2/(2*4.904455^2))
const stat2Fn = (x) => 0.05897603 * Math.exp(-((x - 32.29439) ** 2) / (2 * (4.904455 ** 2)));

const stat2Data = [];

for (let x = 0; x < 101; x++) {
  stat2Data.push(stat2Fn(x));
}

new Chart(document.getElementById('stat-2'), {
  type: 'line',
  data: {
    labels: Array.from({ length: 101 }, (_, index) => `${index} jaar`),
    datasets: [
      {
        label: 'Kans op nieuw levend kind per jaar',
        data: stat2Data.map((val) => (val * 100)), // Multiply by 100 to get percentages
        borderColor: Utils.CHART_COLORS.blue,
        backgroundColor: Utils.transparentize(Utils.CHART_COLORS.blue, 0.5),
      }
    ]
  },
  options: {
    responsive: true,
    scales: {
      x: {
        title: {
          display: true,
          text: 'Leeftijd van de persoon',
        },
      },
      y: {
        min: 0,
        max: 6,
        offset: true,
        title: {
          display: true,
          text: 'Kans op het krijgen van een nieuw levend kind in een jaar tijd',
        },
        ticks: {
          callback: function (value) {
            return value + '%';
          },
        },
      }
    },
    elements: {
      point: {
        radius: 0,
        hoverRadius: 4,
        hitRadius: 4,
      },
      line: {
        tension: 1,
        cubicInterpolationMode: 'monotone', // For smoother lines
      }
    },
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        callbacks: {
          label: function (context) {
            var label = context.dataset.label || '';
            if (context.parsed.y !== null) {
              label += ' ' + context.parsed.y.toFixed(2) + '%';
            }
            return label;
          }
        }
      },
    }
  },
});
