---
title : "Synthetic Data Generation Frontend"
description: "Frontend to control and visualize Synthetic Data Generation"
date: 2023-08-17T09:14:40+02:00
draft: false
toc: true
---

## Running locally

Make sure that the [sdg-event-simulator-person](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-event-simulator-person/) is running, that data is seeded by the [seeder](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/seed-realm), and that the time is running. See the [sdg-event-simulator-person contibuting information](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-event-simulator-person/-/blob/main/contributing.md) for instructions.

Then clone [this repo](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/generator-dashboard). Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

When running for the first time, make sure `pnmp` is installed (e.g. using `brew install pnpm`) and run `pnpm install`.

Run:

```sh
pnpm run dev
go run main.go -port 8080
```

This starts a web server locally on the specified port.
